#!/bin/bash
# stop old container ('|| true' is so that an error from this command is ignored)
docker stop petclinic || true
# remove old container ('|| true' is so that an error from this command is ignored)
docker rm petclinic || true
# start container 
docker run -itd -p 80:8080 --name petclinic npereira/petclinic